// DOM - Document Object Model
//NOTES:
// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows to get a sepcific element

//we can contain the code inside a constant

// alternative
// document.getElementById("txt-first-name")
// document.getElementsByClassName()
// document.getElementsByTagName()


// console.log(document)

/*
   Mini-Activity
    Target the element Full Name: and store it in a aconstant called spanFullName
Solution:
   const spanFullName = document.querySelector('#span-full-name')
*/

const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')



    txtFirstName.addEventListener('keyup', (e) => {
      spanFullName.innerHTML = txtFirstName.value;
    })

    txtFirstName.addEventListener('keyup', (e) => { 
      console.log(e.target)
      console.log(e.target.value)

     })


    txtLastName.addEventListener('keyup', (b) => {
      spanFullName.innerHTML = txtLastName.value;
    })

    txtLastName.addEventListener('keyup', (b) => { 
      console.log(b.target)
      console.log(b.target.value)

     })

//ACTIVITY SOLUTION
 txtLastName.addEventListener ('keyup', (e) => {
      spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
    })




